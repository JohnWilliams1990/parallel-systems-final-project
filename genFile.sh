#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then 
  printf "Usage:\n"
  printf "./$0 {Filename} {Size}\n\n"
  exit 
fi

name=$1
val=$2

if [[ $val =~ ^-?[0-9]+$ ]]; then 
  touch $name
  echo $val > $name
  for i in `seq 0 $(( val * val -1 ))`; do printf "$((1 + RANDOM % 10)) " ; done >> $name
  #for i in `seq 0 3600`; do printf "$((1 + RANDOM % 10)) " ; done
  echo >> $name
  for i in `seq 0 $((val -1))`; do printf "$((1 + RANDOM % 10)) " ; done >> $name  
fi 
