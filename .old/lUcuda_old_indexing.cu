// John Williams 
// Parallel and Distributed systems
//

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <sys/time.h>
#include <cuda.h>

#define TILE 10
using namespace std; 

struct matrix_t{
  double **array;
  double **lower;
  double **upper;
  int dimention; 
};

double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
bool read_in_file(FILE* file, matrix_t * matPtr);
void divide(double ** matrix, double val, int dim);
void decomp(matrix_t * matPtr);
__global__ void multiplyMatrix(double* a, double* b, double* c, int n);
__global__ void LU_cuda(double* a, double* b, double* c, int n);

void multiplyMat(double** ans, double **L ,double **R, int dim);

///////////////////////////////////////////////////////////////////
  //FILE* file = fopen ("A.txt", "r");
  //FILE* file = fopen ("B.txt", "r");
  //FILE* file = fopen ("A1.txt", "r");
int main(int argc, char* argv[]) {
  struct timeval start, end;
  double runtime;
  bool check = false;

  // file is provided on the commandline
  FILE* file = fopen( argv[1], "r");

  struct  matrix_t args;
  struct  matrix_t * matPtr;
  matPtr = & args;

  // scope here is inportant 
  // value of the args exists only inside the while loop
  while (!feof (file)){
  //while (read_in_file(file, &args)){
    check = read_in_file(file, &args);
    if (check == true) {break;}
    printMat(&args, 0);
    gettimeofday( &start, NULL );
    ///////////////////////////////////////////////////////////////////////////////

    decomp(&args);

    ///////////////////////////////////////////////////////////////////////////////
    gettimeofday( &end, NULL );
    runtime = ( ( end.tv_sec  - start.tv_sec ) * 1000.0 ) + ( ( end.tv_usec - start.tv_usec ) / 1000.0 );    
    printf("Program runs in %1.8f seconds\n", runtime);
  free(args.array);
  free(args.lower);
  free(args.upper);
  }
  return 0; 
}
///////////////////////////////////////////////////////////////////

double **alloc_2d_double(int n) {
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
    array[i] = &(data[n*i]);    
  return array;
}

void eyes( double ** matrix, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      if (i == j){
        matrix[i][j] = 1;
      }
      else {
        matrix[i][j] = 0;
      }
    }
  }
}

void divide(double ** matrix, double val, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
        matrix[i][j] = matrix[i][j] / val;
    }
  }
  //printArr(matrix,dim);
}





void cleanupMatrix(double ** arr, int dim){
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

// 0 is array. 1 is lower. 2 is upper
void printMat( matrix_t* matPtr , int mat ){
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention; j++){
      if (mat == 0){
        printf("%1.3f ", matPtr->array[i][j]);
      } else if (mat == 1){
        printf("%1.3f ", matPtr->lower[i][j]);
      } else if (mat == 2){
        printf("%1.3f ", matPtr->upper[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}

void printArr(double **M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    for(int j = 0; j <  mat; j++){
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//void read_in_file(FILE* file, void* arg){
bool read_in_file(FILE* file, matrix_t * matPtr){
  //struct matrix_t *matPtr = (struct matrix_t*) arg;
  int input = 0;
  bool end = false;
  fscanf (file, "%d", &input);    
  //if( fscanf (file, "%d", &input) == EOF){end = true; printf("HERE"); return end; };
  //printf("val: %d\n", input); 
  if (input <= 0){return true; /*exit(0);*/}
  matPtr->dimention = input;
  matPtr->array = alloc_2d_double( matPtr->dimention);
  matPtr->lower = alloc_2d_double( matPtr->dimention);
  matPtr->upper = alloc_2d_double( matPtr->dimention);
  eyes(matPtr->lower,  matPtr->dimention);
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention;  j++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
      matPtr->array[i][j] = input;
      matPtr->upper[i][j] = input;
    }
  }
  // printMat(matPtr, 0);
  // printMat(matPtr, 1);
  // printMat(matPtr, 2);
  return end;
}

// void decomp(matrix_t * matPtr){ 
//   double ** l = alloc_2d_double( matPtr->dimention);
//   eyes(l, matPtr->dimention);
//   eyes(matPtr->lower, matPtr->dimention);
//   double R = 0.0;
//   double Q = 0.0;
//   for(int k = 0; k <  matPtr->dimention ; k++){
//     for(int i = k + 1; i <  matPtr->dimention; i++){
//       eyes(l, matPtr->dimention);
//       R = matPtr->upper[i][k]; 
//       Q = matPtr->upper[k][k]/matPtr->upper[i][k];
//       for(int j = k; j <  matPtr->dimention; j++){
//         eyes(l, matPtr->dimention);
//
//         if (j == k && matPtr->upper[k][k] == 1){
//           l[i][j] = R; 
//         }
//         else if (  j == k && matPtr->upper[k][k] != 1){
//           l[i][j] = 1 / Q; 
//         }
//         if (matPtr->upper[k][k] == 1){
//           matPtr->upper[i][j] = -R * matPtr->upper[k][j] + matPtr->upper[i][j];
//         }
//         else {
//           matPtr->upper[i][j] = (-1/Q) * matPtr->upper[k][j] + matPtr->upper[i][j]; 
//         }
//         multiply(matPtr->lower, matPtr->lower, l, matPtr->dimention);
//       }
//     }
//   }
// }
void decomp(matrix_t * matPtr){ 
  double ** l = alloc_2d_double( matPtr->dimention);
  eyes(l, matPtr->dimention);
  eyes(matPtr->lower, matPtr->dimention);

  multiplyMat(matPtr->array, matPtr->lower, matPtr->upper, matPtr->dimention);

  //multiply(matPtr->lower,matPtr->array ,matPtr->array, matPtr->dimention);


  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("Matrix:\n");
  printMat( matPtr, 0);
  printf("Lower:\n");
  printMat( matPtr, 1);
  printf("Upper\n");
  printMat( matPtr, 2);
  printf("Lower * Upper\n");
  multiply(l, matPtr->lower, matPtr->upper, matPtr->dimention);
  printArr( l, matPtr->dimention);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  free(l);
  //printMat( matPtr, 0);

// %L*A;  
// 
// U = A;
// y = L\b;
// X = L*U\L*y; 
// end
}

void multiplyMat(double** ans, double **L ,double **R, int dim){

  int n = dim;
  int numblock =  n/TILE + ((n%TILE)?1:0);
  dim3 dimGrid(numblock,numblock);
  dim3 dimBlock(TILE,TILE);	 
  double *da, *db, *dc; //device pointers

  //Allocate memory on device
  cudaMalloc((void**)&da, n*n*sizeof(double));
  cudaMalloc((void**)&db, n*n*sizeof(double));
  cudaMalloc((void**)&dc, n*n*sizeof(double));
  //
  cudaMemcpy(da, R[0], n*n*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(db, L[0], n*n*sizeof(double), cudaMemcpyHostToDevice);

  //multiplyMatrix <<<dimGrid,dimBlock>>>(da, db, dc,n);
  LU_cuda <<<dimGrid,dimBlock>>>(da, db, dc,n);
  cudaThreadSynchronize();
  //cudaMemcpy(ans[0],dc, n*n*sizeof(double),cudaMemcpyDeviceToHost); 
  cudaMemcpy(L[0],db, n*n*sizeof(double),cudaMemcpyDeviceToHost); 
  cudaMemcpy(R[0],dc, n*n*sizeof(double),cudaMemcpyDeviceToHost); 
  cudaFree(da);
  cudaFree(db);
  cudaFree(dc);
}

void multiply(double** ans, double **L ,double **R, int dim){
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
}

__global__ void multiplyMatrix(double* a, double* b, double* c, int n){

  __shared__ double A[TILE][TILE];
  __shared__ double B[TILE][TILE];

  int tx = threadIdx.x;
  int ty = threadIdx.y;

//printf("%d\n",ty);
 
  int Row = blockIdx.y*TILE + ty;
  int Col = blockIdx.x*TILE + tx;
  
  double value = 0;

  if (Row < n && Col < n){
    for (int i = 0; i < n ; i += TILE) {
      // Load the matrices from device memory to shared memory
      // Each thread loads one element of each matrix
      A[ty][tx] = a[ n*Row + (i + tx)]; 
      B[ty][tx] = b[ n*(i + ty) + Col]; 
      // Synchronize to make sure the matrices are loaded
      __syncthreads();  

      // Multiply the two matrices
      // Each thread computes one element of the block sub-matrix
      int m = ((n - i) < TILE)? (n - i): TILE;
      for (int j = 0; j < m; j++){
        value += A[ty][j] * B[j][tx];
      }

      // Synchronize to make sure that the preceding computation is done before 
      // loading two new sub-matrices of A and B in the next iteration
      __syncthreads();  
      
    }
    c[Row*n + Col] = value;
  }
}

__device__ void multiply_matrix(double* ans, double *L ,double *R, int dim){
  __shared__ double C[TILE][TILE];
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0;
      for(int k = 0; k < dim; k++){      
        //C[i][j] = C[i][j] + L[i][k] * R[k][j];
        //C[i][j] = C[i][j] + L[dim*i + k] * R[dim*k+j];
        C[i][j] = ans[dim*i + j] +  L[dim*i + k] * R[dim*k+j];
        //printf("%d ",dim*i + j);
        //printf("%f ",C[i][j]);
      }
        printf("\n");
    }
    //ans = C[i];
  }


  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
        //printf("%f ", L[dim*i + j]);
        printf("%f ", ans[dim*i + j]);
    }
    printf("\n");
  }



printf("~~~~~~~~~~~~~~~~~~\n");
//    printf("%f ",R[0]);
//    printf("%f ",R[1]);
//  printf("%f \n",R[2]);
//    printf("%f ",R[0]);
//    printf("%f ",R[1]);
//  printf("%f \n",R[2]);
//    printf("%f ",R[0]);
//    printf("%f ",R[1]);
//    printf("%f ",R[2]);
  //free(C);
}                
__global__ void LU_cuda(double* array, double* Lower, double* Upper, int n){

// L is assumed to be eyes at this point

  __shared__ double A[TILE * TILE];

  __shared__ double L[TILE][TILE];
  __shared__ double U[TILE][TILE];
  __shared__ double l[TILE][TILE];

  int tx = threadIdx.x;
  int ty = threadIdx.y; 
  int Row = blockIdx.y*TILE + ty;
  int Col = blockIdx.x*TILE + tx;

  ///////////////////////////
  //eyes
  if (tx == ty){ l[ty][tx] = 1; } else { l[ty][tx] = 0; }
  double R = 0;
  double Q = 0;
  double value = 0;
  // read in array for A and L 
  // L has eyes
  if (Row < n && Col < n){
    for (int i = 0; i < n ; i += TILE) {
      A[ n*Row + (i + tx)] = array[ n*Row + (i + tx)]; 
      L[ty][tx] = Lower[ n*Row + (i + tx)];
      U[ty][tx] = array[ n*(i + ty) + Col];
       __syncthreads();
    }
  }

  // add code here to do LU decomp. I have eyes function for the l matrix. 
  // I need to pass in a L that has eyes applied

  if (Row < n && Col < n){
    for (int i = 0; i < n ; i += TILE) {

      //eyes 
      if (tx == ty){ l[ty][tx] = 1; } else { l[ty][tx] = 0; }
      __syncthreads();  
      // Each thread computes one element of the block sub-matrix
      int m = ((n - i) < TILE)? (n - i): TILE;
      for (int j = 0; j < m; j++){
        // for kji form j here represents k

        if (tx != 0 && tx >= j+1) { // LOOP 2
          R = U[tx][j];
          Q = U[j][j]/U[tx][j];
          //printf("%d: Q: %f\n",j,Q);
          //printf("%d: R: %f\n",j,R);
          __syncthreads();  
          
          // eyes function
          if (tx == ty){ l[ty][tx] = 1; } else { l[ty][tx] = 0; }
          __syncthreads();

          // order is wrong here  --------------> does this order matter? --> NOPE --> threads are printing out of order
            if ( ty == j && U[j][j] == 1 ){ 
              l[tx][ty] = R;
              //printf("R: l: %f\n",R);
              __syncthreads();
            } else if ( ty == j && U[j][j] != 1){
              l[tx][ty] = 1/Q;
              //printf("1/Q l: %f\n\n",1/Q);
              __syncthreads();
            } 
  
            if ( U[j][j] == 1 ){
              U[tx][ty] = -R * U[j][ty] + U[tx][ty];
              //printf("U[tx][ty] l: %f i: %d j: %d k: %d \n\n", U[tx][ty], tx, ty , j);
              __syncthreads();
            } else {
              U[tx][ty] = (-1/Q) * U[j][ty] + U[tx][ty];
              //printf("U[tx][ty] l: %f\n\n", U[tx][ty]);
              //printf("U[tx][ty] l: %f i: %d j: %d k: %d \n\n", U[tx][ty], tx, ty , j);
              __syncthreads();
            }

          }

         // multiply matrix L * l here

// https://solarianprogrammer.com/2019/03/27/c-programming-passing-multi-dimensional-array-to-function/
        if ( tx == 0 && ty == 0 ){ 
          //multiply_matrix(L,L[0] ,l[0],n);
          multiply_matrix(A,L[0] ,l[0],n);
          for( int i = 0; i < n; i++ ){
            for( int j = 0; j < n; j++ ){
            //printf("%f ",l[i][j]);
            //printf("%f ",L[i][j]);
            //printf("%f ",U[i][j]);
            //printf("%f ",L[i][j]);
            }
            printf("\n");
          }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //for (int i = 0; i < n ; i += TILE) {
        //  __syncthreads();  

        //  // Multiply the two matrices
        //  // Each thread computes one element of the block sub-matrix
        //  int m = ((n - i) < TILE)? (n - i): TILE;
        //  for (int j = 0; j < m; j++){
        //    value += L[ty][j] * l[j][tx];
        //  }

        //  // Synchronize to make sure that the preceding computation is done before 
        //  // loading two new sub-matrices of A and B in the next iteration
        //  __syncthreads();  
        //  
        //}
        //L[tx][ty] = value;






         }
         __syncthreads();
      }

      // Synchronize to make sure that the preceding computation is done before 
      // loading two new sub-matrices of A and l in the next iteration
      __syncthreads();  
    }

    if (Row < n && Col < n){
      for (int i = 0; i < n ; i += TILE) {
        Lower[ n*Row + (i + tx)] = l[ty][tx];
        //Lower[ n*Row + (i + tx)] = L[ty][tx];
        Upper[ n*(i + ty) + Col] = U[ty][tx];
         __syncthreads();
      }
    }
   // Upper[Row*n + Col] = U[tx][ty];
   // Lower[Row*n + Col] = L[tx][ty];

  }
}


// HELPFUL SOURCES CITED
// https://solarianprogrammer.com/2019/03/27/c-programming-passing-multi-dimensional-array-to-function/
