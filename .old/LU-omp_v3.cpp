/**************************************************************************************************
 *  Amanda Suydam: LU decomposition with Open MP
 *  November, 2019
 *  Based of sequential code by John Williams
 *  And then changed to use the LU decomposition on
 *  https://www.tutorialspoint.com/cplusplus-program-to-perform-lu-decomposition-of-any-matrix
 *
 *  for Parallell and Distributed system
 *  **********************************************************************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>
using namespace std;

// Functions declarations
bool read_in_file(FILE* file, double **arr, double **low, double **up, double *b, double *x, int dim);
double **alloc_2d_double(int n);
void printArr(double **M, int mat);
void printVec(double *M, int mat);
void cleanupMatrix(double **arr, int dim);
void multiply(double **C, double **L, double **R, int dim);

/************************* Not sure we need these *************************/
//void divide(double **matrix, double val, int dim);

/* *************************** This is getting changed ********************/
//void decomp(matrix_t *matPtr);
void LUdecomposition(double **arr, double **low, double **up, double *b, double *x, int dim);
void backSubFwdElim(double **low, double **up, double *b, double *x, int dim);

/***************************************************************************************************
                                      MAIN FUNCTION 
 **************************************************************************************************/
int main(int argc, char *argv[]){
    struct timeval start, end;
    double runtimel;
    bool check = false;

    // File provided via command line
    FILE *file;
	// end of the struct
    int num_thread, dim;
	double **arr, **low, **up;
	double *b, *x;
    

    // check input and preven seg fault errors from incorrect file names
    if (argc != 3)
    {
        printf("Usage: ./lu-omp <file name> <number of threads>\n");
        return -1;
    }
    num_thread = atoi(argv[2]);    
    file = fopen(argv[1], "r");

    //omp_set_num_threads(num_thread);
    // scope is important, args only exists in this loop
    while (!feof(file))
    {
		// change this to take arrays instead of struct
        // check = read_in_file(file, &args);
		check = read_in_file(file, arr, low, up, b, x, dim); 
        if (check == true) {break;}
		// change this take the array
        //decomp(&args);
		LUdecomposition(arr, low, up, b, x, dim);
        // above will run everything, after it returns, release arrays
        free(arr);
        free(low);
        free(up);
    }
    return 0;
}
/******************************** FUNCTION DEFINITIONS ***********************************************/


// ********************************************************
//  Allocate the 2-D array
double **alloc_2d_double(int n) 
// Takes in an int n, for an n x n array
// Returns an allocated n x n array
// ********************************************************
{
    double *data = (double *)malloc(n*n*sizeof(double));
    double **array = (double **)malloc(n*sizeof(double*));
    for (int i=0; i<n; i++)
	array[i] = &(data[n*i]);

    return array;
}

//*****************************************************************************
// Read in the matrix from a file
bool read_in_file(FILE* file, double **arr, double **low, double **up, double *b, double *x, int dim)
// this function takes ina  file and a pointer to the matix_t struct. It reads 
// the file and initializes the matPtr object with the contents. If successfule
// it returns false, if it fails to open and initialize it returns true
// ***************************************************************************
{
    int input = 0;
    bool end = false;
    fscanf(file, "%d", &input);

    if(input <= 0) {return true;}

    dim = input;
    arr = alloc_2d_double(dim);
    low = alloc_2d_double(dim);
    up = alloc_2d_double(dim);

    for(int i=0; i<dim; i++)
    {
        for(int j=0; j<dim; j++)
        {
             if(fscanf(file, "%d", &input) == EOF) {end = true;};
             arr[i][j] = input;
             //up[i][j] = input;
        }
    }

    b = (double *)malloc(dim*sizeof(double*));
    x = (double *)malloc(dim*sizeof(double*));
    printf("B: ");
    for(int i=0; i<dim; i++)
    {
        if(fscanf(file, "%d", &input) == EOF) {end=true;};
        b[i] = input;
        printf("%f ", b[i]);
    }
    printf("\n\n");
    return end;
}

//********************************************************************
//Print functions!
// These are fairly self explinatory so I'm bunching them together
// We have a function to print a Matrix, another to print an array
// and the last to print a vector. The differences between are how 
// they get the values passed and the appropriate for loops
// In printMat 0 is array, 1 is lower, 2 is upper
// ******************************************************************

void printArr(double **M, int mat)
{
    for(int i=0; i<mat; i++)
    {
        for(int j=0; j<mat; j++)
        {
            printf("%1.3f ", M[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void printVec(double *M, int mat)
{
    for(int i=0; i<mat; i++)
    {
        printf("%1.3f ", M[i]);
    }
    printf("\n");
}

//***************************************************************************
void LUdecomposition(double **arr, double **low, double **up, double *b, double *x, int dim) 
// Takes in the array arr, the empty arrays low and up for the lower
// and upper arrays of LU decomposition, and the size of all the arrays as dim
// perorm magic math stuff, by the way of coluclations, and then places the 
// appropriate values in low and up. 
// ***************************************************************************
{
	int i = 0, j = 0, k = 0;
    for (i = 0; i < dim; i++) {
		// lower matrix
		for (j = 0; j < dim; j++) {
			if (j < i)
				low[j][i] = 0;
			else {
				low[j][i] = arr[j][i];
				for (k = 0; k < i; k++) {
					low[j][i] = low[j][i] - low[j][k] * up[k][i];
				}
			}
		}
		// upper matrix
		for (j = 0; j < dim; j++) {
			if (j < i)
				up[i][j] = 0;
			else if (j == i)
				up[i][j] = 1;
			else {
				up[i][j] = arr[i][j] / low[i][i];
				for (k = 0; k < i; k++) {
					up[i][j] = up[i][j] - ((low[i][k] * up[k][j]) / low[i][i]);
				}
			}
		}
	}
	
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("Matrix:\n");
    printArr(arr,dim);
    printf("Lower:\n");
    printArr(low,dim);
    printf("Upper:\n");
    printArr(up,dim);
    printf("Lower * Upper:\n");
	// need an array for the answer
	double **ans;
	ans = alloc_2d_double(dim);
    multiply(ans,low, up, dim);
    printArr(ans,dim);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    free(ans);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    backSubFwdElim(low, up, b, x, dim);
    printVec(x, dim);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}
//**************************************************************
// Multiply
void multiply(double **ans, double **L, double **R, int dim)
// Takes in a matrix for answers, as well as the matrices L and 
// R, and the size of all three in the form of int dim
// it creates a temp array C which is initialized with zeros 
// and used to hold the Sum of multiplication. 
// Nothing is returned but the ans array contains the answers
// NOTE: this is a likely candidate for our race conditions.
// ************************************************************
{ 
    double **C = alloc_2d_double(dim);

    for(int i=0; i<dim; i++) 
    {
        for(int j=0; j<dim; j++)
	{
            C[i][j] = 0;
            for(int k=0; k<dim; k++)
            {
                C[i][j] = C[i][j] + L[i][k] * R[k][j];
            }
        }
        ans[i] = C[i];
    }   
    free(C);
}
//**************************************************************
// Back Substitusion and Forward Elimination of the matrix
void backSubFwdElim(double **low, double **up, double *b, double *x, int dim)
// This takes in a pointer to the matrix struct. 
// I'm really afraid this is going to cause problems and race
// conditions because instead of several variables I can 
// divide out by shared and private I have one.
// This functions takes in struct of matrix elements and from those
// uses the dimension ints, lower, and upper arrays, and vectors 
// b and x
// It makes changes to vectors b and x but returns nothing.
// *************************************************************
{
    // forward substitioon
    double s = 0;
    double y[dim];

    for(int k=0; k<dim; k++)
    {
        y[k] = b[k];
    }
    for(int k=0; k<dim; k++)
    {
        for(int i=k+1; i<dim; i++)
        {
            y[i] = y[i] - low[i][k] * y[k];
        }
    }
	
    // backward elimination
    for(int i=dim-1; i>-1; i--)
    {
        s = y[i];
        for(int j=i+1; j<dim; j++)
        {
            s = s - up[i][j] * x[j];
        }
        x[i] = s/up[i][i];
    }
}
//**************************************************************
// Clean up the matrix, as in release it.
void cleanupMatrix(double **arr, int dim)
// Takes in a matrix and it's dimensions
// Frees that matrix from memory row by row
// Returns nothing because it got rid of it all
// *************************************************************
{
    for(int i=0; i<dim; i++)
    {
        double *currentRow = arr[i];
        free(currentRow);
    }
}
