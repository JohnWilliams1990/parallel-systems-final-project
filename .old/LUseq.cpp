// John Williams 
// Parallel and Distributed systems
//

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <sys/time.h>
using namespace std; 

struct matrix_t{
  double **array;
  double **lower;
  double **upper;
  double *b;
  double *x;
  int dimention; 
};

double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );
void printVec(double *M , int mat );
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
//void read_in_file(FILE* file, matrix_t * matPtr);
bool read_in_file(FILE* file, matrix_t * matPtr);
void divide(double ** matrix, double val, int dim);
void decomp(matrix_t * matPtr);
void backSubFwdElim(matrix_t * matPtr);
///////////////////////////////////////////////////////////////////
  //FILE* file = fopen ("A.txt", "r");
  //FILE* file = fopen ("B.txt", "r");
  //FILE* file = fopen ("A1.txt", "r");
int main(int argc, char* argv[]) {
  struct timeval start, end;
  double runtime;
  bool check = false;

  // file is provided on the commandline
  FILE* file = fopen( argv[1], "r");

  struct  matrix_t args;
  struct  matrix_t * matPtr;
  matPtr = & args;

  // scope here is inportant 
  // value of the args exists only inside the while loop
  while (!feof (file)){
  //while (read_in_file(file, &args)){
    check = read_in_file(file, &args);
    if (check == true) {break;}
    //printMat(&args, 0);
    gettimeofday( &start, NULL );
    decomp(&args);
    gettimeofday( &end, NULL );
    runtime = ( ( end.tv_sec  - start.tv_sec ) * 1000.0 ) + ( ( end.tv_usec - start.tv_usec ) / 1000.0 );    
    printf("Program runs in %1.8f ms\n", runtime);
  free(args.array);
  free(args.lower);
  free(args.upper);
  } 
}
///////////////////////////////////////////////////////////////////

double **alloc_2d_double(int n) {
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
    array[i] = &(data[n*i]);    
  return array;
}

void eyes( double ** matrix, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      if (i == j){
        matrix[i][j] = 1;
      }
      else {
        matrix[i][j] = 0;
      }
    }
  }
}

void divide(double ** matrix, double val, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
        matrix[i][j] = matrix[i][j] / val;
    }
  }
  //printArr(matrix,dim);
}

void multiply(double** ans, double **L ,double **R, int dim){
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
  // assignment to ans that is passed in;
  // unsure of how to do assignment with out creating a new array


  // for(int i = 0; i < dim; i++){
  //   ans [i] = C[i];
  // }
}

void cleanupMatrix(double ** arr, int dim){
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

//void backwardSubstatution(matrix_t * matPtr ){
void backSubFwdElim(matrix_t * matPtr ){
  //      1 2  3 
  //  A = 4 5  6 
  //      7 8 10 
  // 
  //      1 0 0
  //  L = 4 1 0 
  //      7 2 1
  //    
  //      1  2  3 
  //  U =   -3 -6 
  //            1
  //
  //  Ly = b --> solve for y 
  //  Ux = y --> solve for x 
  //  
  //  forward substitution
  //
  //  1 0 0 | 1      1
  //  4 1 0 | 2 --> -2
  //  7 2 1 | 3      0
  //  
  //  backward elimination
  //
  //  1  2  3 |  1     -1/3
  //    -3 -6 | -2 -->  2/3
  //        1 |  0        0

  //  forward substitution
  //
  // for k = 1 to n - 1 do
  //   for i = k + 1 to n do
  //     bi = bi - aik * bk
  //   endfor
  // endfor

  //  backward elimination

  // for i = n downto 1 do
  //   s = bi
  //   for j = i + 1 to n do
  //     s = s -  aij * xj
  //   endfor
  //   xi = s/aii
  // endfor

  double s = 0; 
  double y [matPtr->dimention]; 

  for (int k = 0;  k <  matPtr->dimention ; k++) {
    y[k] = matPtr->b[k];
  }
  // Ly = b
  for (int k = 0;  k <  matPtr->dimention; k++) {
    for (int i = k + 1; i < matPtr->dimention; i++) {
      //bi = bi - aik bk
      y[i] = y[i] -  matPtr->lower[i][k] * y[k];
    }
  }
  //for (int k = 0;  k <  matPtr->dimention ; k++) {
  //  printf("%f ", y[k]);
  //}
  //printf("\n\n");

  //// Ux = y 

  for(int i = matPtr->dimention-1; i > -1 ; i--){
    s = y[i];
    for (int j = i +1 ; j <  matPtr->dimention; j++){
      s = s - matPtr->upper[i][j] *  matPtr->x[j];
    }
    matPtr->x[i] = s / matPtr->upper[i][i];
  }
  //printf("\n\n");
  //for (int k = 0;  k <  matPtr->dimention ; k++) {
  //  printf("%f ", matPtr->x[k]);
  //}
  //printf("\n\n");
}

void decomp(matrix_t * matPtr){ 
  double ** l = alloc_2d_double( matPtr->dimention);
  eyes(l, matPtr->dimention);
  eyes(matPtr->lower, matPtr->dimention);
  double R = 0.0;
  double Q = 0.0;
  for(int k = 0; k <  matPtr->dimention ; k++){
    for(int i = k + 1; i <  matPtr->dimention; i++){
      eyes(l, matPtr->dimention);
      R = matPtr->upper[i][k]; 
      Q = matPtr->upper[k][k]/matPtr->upper[i][k];
      //printf("%d: Q: %f\n",i,Q);
      //printf("Q: %f i: %d k: %d \n", Q, i,k);
      //printf("R: %f\n",R);
      for(int j = k; j <  matPtr->dimention; j++){
      //printf("upper[k][k] %f upper[i][k] %f I,J,K: %d %d %d\n",matPtr->upper[k][k],matPtr->upper[i][k],i,j,k);
      //printf("I,J,K: %d %d %d\n",i,j,k);
        eyes(l, matPtr->dimention);
        if (j == k && matPtr->upper[k][k] == 1){
          l[i][j] = R; 
          //printf("R: l: %f\n",R);
        }
        else if (  j == k && matPtr->upper[k][k] != 1){
          //printf("upper[k][k] %f upper[i][k] %f Q  %f I,J,K: %d %d %d\n",matPtr->upper[k][k],matPtr->upper[i][k],Q,i,j,k);
          l[i][j] = 1 / Q; 
          //printf("1/Q l: %f\n\n",1/Q);
        }
        if (matPtr->upper[k][k] == 1){
          //printf("R: %f\n",R);
          //printf("upper[k][k] %f upper[i][k] %f R  %f I,J,K: %d %d %d\n",matPtr->upper[k][k],matPtr->upper[i][k],R,i,j,k);
          matPtr->upper[i][j] = -R * matPtr->upper[k][j] + matPtr->upper[i][j];
          //printf("U[tx][ty] l: %f\n\n",matPtr->upper[i][j]);
          //printf("U[tx][ty] l: %f i: %d j: %d k: %d \n\n", matPtr->upper[i][j], i, j , k);
        }
        else {
         //printf("upper[k][k] %f upper[i][k] %f U  %f I,J,K: %d %d %d\n",matPtr->upper[k][k],matPtr->upper[i][k],Q,i,j,k);
          //printf("Q: %f i: %d j: %d k: %d \n", Q, i,j,k);
          //printf("upper[k][k] %f upper[i][k] %f Q  %f I,J,K: %d %d %d\n",matPtr->upper[k][k],matPtr->upper[i][k],Q,i,j,k);
          matPtr->upper[i][j] = (-1/Q) * matPtr->upper[k][j] + matPtr->upper[i][j]; 
          //printf("U[tx][ty] l: %f\n\n",matPtr->upper[i][j]);
          //printf("U[tx][ty] l: %f i: %d j: %d k: %d \n\n", matPtr->upper[i][j], i, j , k);
        }
       
       //printArr( l, matPtr->dimention);
       
       //printf("I,J,K: %d %d %d\n",i,j,k);
       multiply(matPtr->lower, matPtr->lower, l, matPtr->dimention);
       //printMat( matPtr, 1);
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
      }
    }
  }

  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("Matrix:\n");
  printMat( matPtr, 0);
  printf("Lower:\n");
  printMat( matPtr, 1);
  printf("Upper\n");
  printMat( matPtr, 2);
  printf("Lower * Upper\n");
  multiply(l, matPtr->lower, matPtr->upper, matPtr->dimention);
  printArr( l, matPtr->dimention);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  free(l);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  backSubFwdElim( matPtr );
  printVec( matPtr->x, matPtr->dimention);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
   


  //printMat( matPtr, 0);

// %L*A;  
// 
// U = A;
// y = L\b;
// X = L*U\L*y; 
// end
}

// 0 is array. 1 is lower. 2 is upper
void printMat( matrix_t* matPtr , int mat ){
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention; j++){
      if (mat == 0){
        printf("%1.3f ", matPtr->array[i][j]);
      } else if (mat == 1){
        printf("%1.3f ", matPtr->lower[i][j]);
      } else if (mat == 2){
        printf("%1.3f ", matPtr->upper[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}

void printArr(double **M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    for(int j = 0; j <  mat; j++){
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}
void printVec(double *M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    printf("%1.3f ", M[i]);
  }
  printf("\n");
}


bool read_in_file(FILE* file, matrix_t * matPtr){
  int input = 0;
  bool end = false;
  fscanf (file, "%d", &input);    
  //if( fscanf (file, "%d", &input) == EOF){end = true; printf("HERE"); return end; };
  //printf("val: %d\n", input); 
  if (input <= 0){ return true; /*exit(0);*/}
  matPtr->dimention = input;
  matPtr->array = alloc_2d_double( matPtr->dimention);
  matPtr->lower = alloc_2d_double( matPtr->dimention);
  matPtr->upper = alloc_2d_double( matPtr->dimention);
  eyes(matPtr->lower,  matPtr->dimention);
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention;  j++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
      matPtr->array[i][j] = input;
      matPtr->upper[i][j] = input;
    }
  }
   
  matPtr->b = (double *)malloc( matPtr->dimention*sizeof(double*));
  matPtr->x = (double *)malloc( matPtr->dimention*sizeof(double*));
  //printf("B: ");
  for(int i = 0; i <  matPtr->dimention; i++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
     matPtr->b[i] = input;
     //printf("%f ",matPtr->b[i]);
  }
     //printf("\n\n ");
  // printMat(matPtr, 0);
  // printMat(matPtr, 1);
  // printMat(matPtr, 2);
  return end;
}

