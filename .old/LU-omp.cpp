// John Williams 
// Parallel and Distributed systems
//
// Edited by Amanda Suydam for OpenMP Parallel programming
// October 28, 2019

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>
using namespace std; 

struct matrix_t{
  double **array;
  double **lower;
  double **upper;
  int dimension; 
};

/************************* FUNCTIONS DECLARATIONS *******************/
double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );  	
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
bool read_in_file(FILE* file, matrix_t * matPtr);
void decomp(matrix_t * matPtr);

/************************** MAIN FUNCTION ****************************/
int main(int argc, char* argv[]) {
  struct timeval start, end;
  double runtime;
  bool check = false;

  // file is provided on the commandline
  if (argc != 3){
    printf("Usage: ./lu-omp <file name> <number of threads>\n");
    std::cout << argc << std::endl;
    return -1;
  }	  
  FILE* file = fopen( argv[1], "r");
  int num_thread = atoi(argv[2]);
  struct  matrix_t args;
  struct  matrix_t * matPtr;
  matPtr = & args;

  omp_set_num_threads(num_thread);
  runtime = omp_get_wtime();
  // scope here is inportant 
  // value of the args exists only inside the while loop
  while (!feof (file)){
    check = read_in_file(file, &args);
    if (check == true) {break;}
      decomp(&args);  // why not send matPtr?
  // There was timing stuff commented out in this while loop I need to replace
  runtime = omp_get_wtime() - runtime;
  std::cout << "LU decomposition with OpenMP runs in " << runtime << std::endl;
  free(args.array);
  free(args.lower);
  free(args.upper);
  } 
}

/**************************** FUNCTIONS *****************************/

/********************************************************************/
double **alloc_2d_double(int n) {
/*  Creates a 2d array of size n*n
 *  Creates a 1d array of size n
 *  Takes in: interger n
 *  returns: a pointer to a 1d array of size n
 *  *****************************************************************/
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
    array[i] = &(data[n*i]);    
  return array;
}

/*******************************************************************/
void eyes( double ** matrix, int dim){
/*
 *
 *
 * ****************************************************************/
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      if (i == j){
        matrix[i][j] = 1;
      }
      else {
        matrix[i][j] = 0;
      }
    }
  }
}

/*******************************************************************/
void multiply(double** ans, double **L ,double **R, int dim){
/*
 *
 *
 * ****************************************************************/
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
}

/*******************************************************************/
void cleanupMatrix(double ** arr, int dim){
/*
 *
 *
 * *****************************************************************/
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

/*******************************************************************/
void decomp(matrix_t * matPtr){ 
/*
 *
 *
 * *****************************************************************/
  double ** l = alloc_2d_double( matPtr->dimension);
  //eyes(l, matPtr->dimension);
  //eyes(matPtr->lower, matPtr->dimension);
  double R = 0.0;
  double Q = 0.0;
 
  // what needs to be shared and what's private? 	
  #pragma omp parallel shared(matPtr ) private(R, Q )
  {
    for(int k = 0; k <  matPtr->dimension ; k++){  // for column starting at zero
      #pragma omp for schedule(dynamic)
      for(int i = k + 1; i <  matPtr->dimension; i++){   // for row starting at 1
        //eyes(l, matPtr->dimension);
        R = matPtr->upper[i][k]; 
        Q = matPtr->upper[k][k]/R; //matPtr->upper[i][k];   // to here can be easily parallelized

        for(int j = k; j <  matPtr->dimension; j++){
          eyes(l, matPtr->dimension);    // this is the one that's needed for this to work
          if (j == k && matPtr->upper[k][k] == 1){
            l[i][j] = R; 
          }
          else if (  j == k && matPtr->upper[k][k] != 1){
            l[i][j] = 1 / Q; 
          } 
          if (matPtr->upper[k][k] == 1){
            matPtr->upper[i][j] = -R * matPtr->upper[k][j] + matPtr->upper[i][j];
          }
          else {
            matPtr->upper[i][j] = (-1/Q) * matPtr->upper[k][j] + matPtr->upper[i][j]; 
          }
          multiply(matPtr->lower, matPtr->lower, l, matPtr->dimension);
        }
      }
    }
  }

  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("Matrix:\n");
  printMat( matPtr, 0);
  printf("Lower:\n");
  printMat( matPtr, 1);
  printf("Upper\n");
  printMat( matPtr, 2);
  printf("Lower * Upper\n");
  multiply(l, matPtr->lower, matPtr->upper, matPtr->dimension);
  printArr( l, matPtr->dimension);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  free(l);
  //printMat( matPtr, 0);

// %L*A;  
// 
// U = A;
// y = L\b;
// X = L*U\L*y; 
// end
}

// 0 is array. 1 is lower. 2 is upper
void printMat( matrix_t* matPtr , int mat ){
  for(int i = 0; i <  matPtr->dimension; i++){
    for(int j = 0; j <  matPtr->dimension; j++){
      if (mat == 0){
        printf("%1.3f ", matPtr->array[i][j]);
      } else if (mat == 1){
        printf("%1.3f ", matPtr->lower[i][j]);
      } else if (mat == 2){
        printf("%1.3f ", matPtr->upper[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}

void printArr(double **M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    for(int j = 0; j <  mat; j++){
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

bool read_in_file(FILE* file, matrix_t * matPtr){
  int input = 0;
  bool end = false;
  fscanf (file, "%d", &input);    
  //if( fscanf (file, "%d", &input) == EOF){end = true; printf("HERE"); return end; };
  //printf("val: %d\n", input); 
  if (input <= 0){ return true; /*exit(0);*/}
  matPtr->dimension = input;
  matPtr->array = alloc_2d_double( matPtr->dimension);
  matPtr->lower = alloc_2d_double( matPtr->dimension);
  matPtr->upper = alloc_2d_double( matPtr->dimension);
  eyes(matPtr->lower,  matPtr->dimension);
  for(int i = 0; i <  matPtr->dimension; i++){
    for(int j = 0; j <  matPtr->dimension;  j++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
      matPtr->array[i][j] = input;
      matPtr->upper[i][j] = input;
    }
  }
  return end;
}

