/**************************************************************************************************
 *  Amanda Suydam: LU decomposition with Open MP
 *  November, 2019
 *  Based of sequential code by John Williams
 *
 *  for Parallell and Distributed system
 *  **********************************************************************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>
using namespace std;

// John is making a struct and I think that might be part of the problem
struct matrix_t{
    double **array;
    double **lower;
    double **upper;
    double *b;
    double *x;
    int dimensions;
};

// Functions declarations
double **alloc_2d_double(int n);
void eyes(double **matrix, int dim);
void printMat(struct matrix_t*, int mat);
void printArr(double **M, int mat);
void printVec(double *M, int mat);
void multiply(double **C, double **L, double **R, int dim);
void divide(double **matrix, double val, int dim);
void cleanupMatrix(double **arr, int dim);
void decomp(matrix_t *matPtr);
bool read_in_file(FILE *file, matrix_t *matPtr);

// MAIN FUNCTION *************************************************************************************/
int main(int argc, char *argv[]){
    struct timeval start, end;
    double runtime;
    bool check = false;

    // File provided via command line
    FILE *file;
    int num_thread;
    struct matrix_t args;
    struct matrix_t *matPtr;
    matPtr = &args; 

    // check input and preven seg fault errors from incorrect file names
    if (argc != 3)
    {
        printf("Usage: ./lu-omp <file name> <number of threads>\n");
        return -1;
    }
    num_thread = atoi(argv[2]);    
    file = fopen(argv[1], "r");

    omp_set_num_threads(num_thread);
    // scope is important, args only exists in this loop
    while (!feof(file))
    {
        check = read_in_file(file, &args);
        if (check == true) {break;}
        gettimeofday( &start, NULL );
        decomp(&args);
        gettimeofday( &end, NULL );
        runtime = ( ( end.tv_sec  - start.tv_sec ) * 1000.0 ) + ( ( end.tv_usec - start.tv_usec ) / 1000.0 );    
        printf("Program runs in %1.8f ms\n", runtime);
        // above will run everything, after it returns, release arrays
        free(args.array);
        free(args.lower);
        free(args.upper);
    }
    return 0;
}

/******************************** FUNCTION DEFINITIONS ***********************************************/
// ********************************************************
//  Allocate the 2-D array
double **alloc_2d_double(int n) 
// Takes in an int n, for an n x n array
// Returns an allocated n x n array
// ********************************************************
{
    double *data = (double *)malloc(n*n*sizeof(double));
    double **array = (double **)malloc(n*sizeof(double*));
    for (int i=0; i<n; i++)
	array[i] = &(data[n*i]);

    return array;
}

//**********************************************************
// initialize the eyes array of zeros and ones
void eyes(double **matrix, int dim)
// takes in a pointer to an array and the integer for the 
//  size of the array. Changes are made to passed array
//  and nothing is returned. The array however will live 
//  forever with the hell it saw. Eyes... so many eyes...
//  ********************************************************
{
    for (int i=0; i<dim; i++)
    {
	for (int j=0; j<dim; j++)
        {
            if (i==j) 
	    {
                matrix[i][j] = 1;
	    }
	    else
	    {
		matrix[i][j] = 0;
            }
	}
    }
} // it's only functions like this I regret my curly boi placement

//*************************************************************
// Divide the elements of the array by a value
void divide(double **matrix, double val, int dim)
// takes in a pointer to a mtrix, the dimension of the matrix
// as a int, and a value. 
// Divides the elements of the matrix by the passed value.
// Returns nothing, hence the void.
// ************************************************************
{
    for(int i=0; i<dim; i++)
    {
        for(int j=0; j<dim; j++)
        {
            matrix[i][j] = matrix[i][j]/val;
        }
    }
}

//**************************************************************
// Multiply
void multiply(double **ans, double **L, double **R, int dim)
// Takes in a matrix for answers, as well as the matrices L and 
// R, and the size of all three in the form of int dim
// it creates a temp array C which is initialized with zeros 
// and used to hold the Sum of multiplication. 
// Nothing is returned but the ans array contains the answers
// NOTE: this is a likely candidate for our race conditions.
// ************************************************************
{ 
    double **C = alloc_2d_double(dim);

    for(int i=0; i<dim; i++) 
    {
        for(int j=0; j<dim; j++)
	{
            C[i][j] = 0;
            for(int k=0; k<dim; k++)
            {
                C[i][j] = C[i][j] + L[i][k] * R[k][j];
            }
        }
        ans[i] = C[i];
    }   
    free(C);
}

//**************************************************************
// Clean up the matrix, as in release it.
void cleanupMatrix(double **arr, int dim)
// Takes in a matrix and it's dimensions
// Frees that matrix from memory row by row
// Returns nothing because it got rid of it all
// *************************************************************
{
    for(int i=0; i<dim; i++)
    {
        double *currentRow = arr[i];
        free(currentRow);
    }
}

//**************************************************************
// Back Substitusion and Forward Elimination of the matrix
void backSubFwdElim(matrix_t *matPtr)
// This takes in a pointer to the matrix struct. 
// I'm really afraid this is going to cause problems and race
// conditions because instead of several variables I can 
// divide out by shared and private I have one.
// This functions takes in struct of matrix elements and from those
// uses the dimension ints, lower, and upper arrays, and vectors 
// b and x
// It makes changes to vectors b and x but returns nothing.
// *************************************************************
{
    // forward substitioon
    double s = 0;
    double y[matPtr->dimensions];

    for(int k=0; k<matPtr->dimensions; k++)
    {
        y[k] = matPtr->b[k];
    }
    for(int k=0; k<matPtr->dimensions; k++)
    {
        for(int i=k+1; i<matPtr->dimensions; i++)
        {
            y[i] = y[i] - matPtr->lower[i][k] * y[k];
        }
    }
	
    // backward elimination
    for(int i=matPtr->dimensions-1; i>-1; i--)
    {
        s = y[i];
        for(int j=i+1; j<matPtr->dimensions; j++)
        {
            s = s - matPtr->upper[i][j] * matPtr->x[j];
        }
        matPtr->x[i] = s/matPtr->upper[i][i];
    }
}

//****************************************************************
// The important one! LU decomposition
void decomp(matrix_t *matPtr)
// Takes in that matrix struct again of all the bits we need 
// It uses from the struct the arrays upper and lower and the int
// dimensions
// It does math on them and returns nothing. To see what math it does
// please refer the to documentation
// ***************************************************************
{
    double **l = alloc_2d_double(matPtr->dimensions);
    double R = 0.0;
    double Q = 0.0;
    eyes(l, matPtr->dimensions);
    eyes(matPtr->lower, matPtr->dimensions);
    int i,k,j;

    //#pragma omp parallel shared(matPtr) firstprivate(k, l) private(R, Q, i, j)  // i,j,k may need to be shared or private
    #pragma omp parallel shared(matPtr) firstprivate(l) private(R, Q, i, j, k)  // i,j,k may need to be shared or private
    //#pragma omp parallel firstprivate(l,U) private(R, Q, i, j, k)  // i,j,k may need to be shared or private
    {
    for(k=0; k<matPtr->dimensions; k++){
	//#pragma omp for ordered //schedule(static) 
	//#pragma omp for ordered //schedule(static) 
        #pragma omp for ordered schedule(static,1)
        for(i=k+1; i<matPtr->dimensions; i++)
        {
            //#pragma omp ordered
            #pragma omp critical
            {
            eyes(l, matPtr->dimensions);      // is this one of the redundant ones?
            R = matPtr->upper[i][k];
            Q = matPtr->upper[k][k]/matPtr->upper[i][k];
            //#pragma omp for schedule(static)
            }
            #pragma omp ordered
            for(j=k; j<matPtr->dimensions; j++)
            {
                
                //#pragma omp critical 
                eyes(l, matPtr->dimensions); 
		// making lower assignment critical resulted in l == eyes
                if(j==k && matPtr->upper[k][k]==1)
                {
                    #pragma omp critical 
                    l[i][j] = R;
                } 
                else if(j==k && matPtr->upper[k][k] !=1)
                {
                    #pragma omp critical 
                    l[i][j] = 1/Q;
                }

                if(matPtr->upper[k][k]==1)
                {
                    #pragma omp critical 
                    matPtr->upper[i][j] = -R * matPtr->upper[k][j] + matPtr->upper[i][j];
                }
                else
                {
                    #pragma omp critical 
                    matPtr->upper[i][j] = (-1/Q) * matPtr->upper[k][j] + matPtr->upper[i][j];
                }
		// each thread is calling this function... this may be the problem
                  //#pragma omp ordered 
                #pragma omp critical 
                {
                  //printf("I,J,K: %d %d %d\n",i,j,k);
                  multiply(matPtr->lower, matPtr->lower, l, matPtr->dimensions);
                }
                //}
            }
        }
    }
    }
    //multiply(matPtr->lower, matPtr->lower, l, matPtr->dimensions);

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("Matrix:\n");
    printMat(matPtr,0);
    printf("Lower:\n");
    printMat(matPtr,1);
    printf("Upper:\n");
    printMat(matPtr,2);
    printf("Lower * Upper:\n");
    multiply(l,matPtr->lower,matPtr->upper,matPtr->dimensions);
    printArr(l,matPtr->dimensions);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    free(l);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    backSubFwdElim(matPtr);
    printVec(matPtr->x, matPtr->dimensions);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}

//********************************************************************
//Print functions!
// These are failry self explinatory so I'm bunching them together
// We have a function to print a Matrix, another to print an array
// and the last to print a vector. The differences between are how 
// they get the values passed and the appropriate for loops
// In printMat 0 is array, 1 is lower, 2 is upper
// ******************************************************************

void printMat(matrix_t *matPtr, int mat)
{
   for(int i=0; i<matPtr->dimensions; i++) 
   {
       for(int j=0; j<matPtr->dimensions; j++) 
       {
           if(mat==0)
               printf("%1.3f ", matPtr->array[i][j]);
           else if(mat==1)
               printf("%1.3f ", matPtr->lower[i][j]);
           else if(mat==2)
               printf("%1.3f ", matPtr->upper[i][j]);    
       }
       printf("\n");
   } 
   printf("\n");
}

void printArr(double **M, int mat)
{
    for(int i=0; i<mat; i++)
    {
        for(int j=0; j<mat; j++)
        {
            printf("%1.3f ", M[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void printVec(double *M, int mat)
{
    for(int i=0; i<mat; i++)
    {
        printf("%1.3f ", M[i]);
    }
    printf("\n");
}

//*****************************************************************************
// Read in the matrix from a file
bool read_in_file(FILE* file, matrix_t *matPtr)
// this function takes ina  file and a pointer to the matix_t struct. It reads 
// the file and initializes the matPtr object with the contents. If successfule
// it returns false, if it fails to open and initialize it returns true
// ***************************************************************************
{
    int input = 0;
    bool end = false;
    fscanf(file, "%d", &input);

    if(input <= 0) {return true;}

    matPtr->dimensions = input;
    matPtr->array = alloc_2d_double(matPtr->dimensions);
    matPtr->lower = alloc_2d_double(matPtr->dimensions);
    matPtr->upper = alloc_2d_double(matPtr->dimensions);
    eyes(matPtr->lower, matPtr->dimensions);

    for(int i=0; i<matPtr->dimensions; i++)
    {
        for(int j=0; j<matPtr->dimensions; j++)
        {
             if(fscanf(file, "%d", &input) == EOF) {end = true;};
             matPtr->array[i][j] = input;
             matPtr->upper[i][j] = input;
        }
    }

    matPtr->b = (double *)malloc(matPtr->dimensions*sizeof(double*));
    matPtr->x = (double *)malloc(matPtr->dimensions*sizeof(double*));
    printf("B: ");
    for(int i=0; i<matPtr->dimensions; i++)
    {
        if(fscanf(file, "%d", &input) == EOF) {end=true;};
        matPtr->b[i] = input;
        printf("%f ", matPtr->b[i]);
    }
    printf("\n\n");
    return end;
}
