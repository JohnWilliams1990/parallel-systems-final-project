/**************************************************************************************************
 *  Comparison of LU Decomposition with MPI 
 *  This code includes the solving of vector X from given B
 *  John Williams, Greg Wicklund, Brian Sumner, Ryan Vacca and
 *  Amanda Suydam
 *  November, 2019
 *  Based of sequential code by John Williams
 *  
 *  for Parallell and Distributed system
 *  **********************************************************************************************/

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <sys/time.h>
#include "mpi.h"
#include <iomanip>
#include <cmath>
using namespace std; 


//Message id's type for worker and master
#define MASTER 0               
#define FROM_MASTER 1          
#define FROM_WORKER 2


struct matrix_t{
  double **array;
  double **lower;
  double **upper;
  double *b;
  double *x;
  int dimention; 
};

//Function declerations
double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
void eyesMPI(matrix_t * matPtr, double ** l, int offset, int rows);
void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );
void printVec(double *M , int mat );
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
bool read_in_file(FILE* file, matrix_t * matPtr);
void divide(double ** matrix, double val, int dim);
void decomp(matrix_t * matPtr, int numtasks, int taskid, int numworkers, MPI_Status status,
            int averow, int extra, int offset);

// MAIN FUNCTION *************************************************************************************/
int main(int argc, char* argv[]) 
{
  struct timeval start, end;
  double runtime;
  bool check = false;

  // Number of processes, process id and worker's
  int numtasks;              
  int taskid;                
  int numworkers;            

  // Rows per slice, extra row per worker and 
  int averow;
  int extra;
  int offset;

  MPI_Status status;
  MPI_Init(NULL,NULL);
  MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
  MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
  numworkers = numtasks-1;

  FILE* file = fopen( argv[1], "r");

  struct  matrix_t args;
  struct  matrix_t * matPtr;
  matPtr = & args;


  while (!feof (file))
  {
    check = read_in_file(file, &args);
    if(check == true) 
    {
      break;
    }
    averow = matPtr->dimention/numworkers;  
    extra = matPtr->dimention%numworkers;   
    decomp(&args, numtasks, taskid, numworkers, status, averow, extra, offset);
    free(args.array);
    free(args.lower);
    free(args.upper);
  } 
}
///////////////////////////////////////////////////////////////////

double **alloc_2d_double(int n) 
{
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
  {
    array[i] = &(data[n*i]);    
  }
  return array;
}

void eyes( double ** matrix, int dim)
{
  for(int i = 0; i < dim; i++)
  {
    for(int j = 0; j < dim; j++)
    {
      if (i == j)
      {
        matrix[i][j] = 1;
      }
      else 
      {
        matrix[i][j] = 0;
      }
    }
  }
}

void divide(double ** matrix, double val, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
        matrix[i][j] = matrix[i][j] / val;
    }
  }
  //printArr(matrix,dim);
}

void multiply(double** ans, double **L ,double **R, int dim){
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
}

void cleanupMatrix(double ** arr, int dim){
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

//void backwardSubstatution(matrix_t * matPtr ){
void backSubFwdElim(matrix_t * matPtr ){

  double s = 0; 
  double y [matPtr->dimention]; 

  for (int k = 0;  k <  matPtr->dimention ; k++) {
    y[k] = matPtr->b[k];
  }
  // Ly = b
  for (int k = 0;  k <  matPtr->dimention; k++) {
    for (int i = k + 1; i < matPtr->dimention; i++) {
      //bi = bi - aik bk
      y[i] = y[i] -  matPtr->lower[i][k] * y[k];
    }
  }

  for(int i = matPtr->dimention-1; i > -1 ; i--){
    s = y[i];
    for (int j = i +1 ; j <  matPtr->dimention; j++){
      s = s - matPtr->upper[i][j] *  matPtr->x[j];
    }
    matPtr->x[i] = s / matPtr->upper[i][i];
  }
}

void decomp(matrix_t * matPtr, int numtasks, int taskid, int numworkers, MPI_Status status,
            int averow, int extra, int offset)
{ 
  int source;                /* task id of message source */
  int dest;                  /* task id of message destination */
  int mtype;                 /* message type */
  int rows;
  double runtime;

  double ** l = alloc_2d_double( matPtr->dimention);

  //Get start time
  if (taskid == MASTER)
  {
    runtime = MPI_Wtime();
  }

  /**************************** master task ************************************/
  if (taskid == MASTER)
  {
      offset = 0;
      mtype = FROM_MASTER;
      for (int dest=1; dest<=numworkers; dest++)
      {   
         if(dest <= extra)
         {
          rows = averow+1;
         }  
         else
         {
          rows = averow;
         }
         MPI_Send(&offset,              1,        MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&rows,                1,        MPI_INT, dest, mtype, MPI_COMM_WORLD);
         offset = offset + rows;
      }

      /* Receive results from worker tasks */
      mtype = FROM_WORKER;
      for (int i=1; i<=numworkers; i++)
      {
         source = i;
         MPI_Recv(&offset, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&rows, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&(l[offset][0]), rows*matPtr->dimention, MPI_DOUBLE, source, mtype, MPI_COMM_WORLD, &status);
      }
  }

  /**************************** worker task ************************************/
  if (taskid > MASTER)
  {
      mtype = FROM_MASTER;
      MPI_Recv(&offset,          1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&rows,            1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);

      eyesMPI(matPtr, l, offset, rows);

      mtype = FROM_WORKER;
      MPI_Send(&offset,               1,        MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&rows,                 1,        MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&(l[offset][0]), rows*matPtr->dimention, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
      
  }

  /**************************** master task ************************************/
  if (taskid == MASTER)
  {
      offset = 0;
      mtype = FROM_MASTER;
      for (int dest=1; dest<=numworkers; dest++)
      {   
         if(dest <= extra)
         {
          rows = averow+1;
         }  
         else
         {
          rows = averow;
         }
         MPI_Send(&offset,              1,        MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&rows,                1,        MPI_INT, dest, mtype, MPI_COMM_WORLD);
         offset = offset + rows;
      }

      /* Receive results from worker tasks */
      mtype = FROM_WORKER;
      for (int i=1; i<=numworkers; i++)
      {
         source = i;
         MPI_Recv(&offset, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&rows, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&(l[offset][0]), rows*matPtr->dimention, MPI_DOUBLE, source, mtype, MPI_COMM_WORLD, &status);
      }
  }

  /**************************** worker task ************************************/
  if (taskid > MASTER)
  {
      mtype = FROM_MASTER;
      MPI_Recv(&offset,          1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&rows,            1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);

      eyesMPI(matPtr, l, offset, rows);

      mtype = FROM_WORKER;
      MPI_Send(&offset,               1,        MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&rows,                 1,        MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&(matPtr->lower[offset][0]), rows*matPtr->dimention, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      
  }

  double R = 0.0;
  double Q = 0.0;

  for(int k = 0; k <  matPtr->dimention ; k++)
  {
    for(int i = k + 1; i <  matPtr->dimention; i++)
    {
      R = matPtr->upper[i][k]; 
      Q = matPtr->upper[k][k]/matPtr->upper[i][k];
      for(int j = k; j <  matPtr->dimention; j++)
      {
        eyes(l, matPtr->dimention);
        

        if (j == k && matPtr->upper[k][k] == 1)
        {
          l[i][j] = R; 
        }
        else if (  j == k && matPtr->upper[k][k] != 1)
        {
          l[i][j] = 1 / Q; 
        }
        if (matPtr->upper[k][k] == 1)
        {
          matPtr->upper[i][j] = -R * matPtr->upper[k][j] + matPtr->upper[i][j];
        }
        else 
        {
          matPtr->upper[i][j] = (-1/Q) * matPtr->upper[k][j] + matPtr->upper[i][j]; 
        }

        multiply(matPtr->lower, matPtr->lower, l, matPtr->dimention);
      }
    }
  }

  /**************************** master task ************************************/
  if (taskid == MASTER)
  {
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("Matrix:\n");
    printMat( matPtr, 0);
    printf("Lower:\n");
    printMat( matPtr, 1);
    printf("Upper\n");
    printMat( matPtr, 2);
    printf("Lower * Upper\n");
    multiply(l, matPtr->lower, matPtr->upper, matPtr->dimention);
    printArr( l, matPtr->dimention);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    free(l);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    backSubFwdElim( matPtr );
    printVec( matPtr->x, matPtr->dimention);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    runtime = MPI_Wtime() - runtime;
    //cout << "Program runs in " << runtime << " ms "<< endl;
    printf("Program runs in %1.8f ms\n", runtime);
  }
  MPI_Finalize();
}



// 0 is array. 1 is lower. 2 is upper
void printMat( matrix_t* matPtr , int mat ){
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention; j++){
      if (mat == 0){
        printf("%1.3f ", matPtr->array[i][j]);
      } else if (mat == 1){
        printf("%1.3f ", matPtr->lower[i][j]);
      } else if (mat == 2){
        printf("%1.3f ", matPtr->upper[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}

void printArr(double **M , int mat )
{
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++)
  {
    for(int j = 0; j <  mat; j++)
    {
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}
void printVec(double *M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    printf("%1.3f ", M[i]);
  }
  printf("\n");
}


bool read_in_file(FILE* file, matrix_t * matPtr)
{
  int input = 0;
  bool end = false;
  fscanf (file, "%d", &input);    

  if (input <= 0)
  { 
    return true; /*exit(0);*/
  }

  // Allocate mmemory for the 3 matrix we use
  // Read in line 1 from file which is matrix size
  matPtr->dimention = input;
  matPtr->array = alloc_2d_double( matPtr->dimention);
  matPtr->lower = alloc_2d_double( matPtr->dimention);
  matPtr->upper = alloc_2d_double( matPtr->dimention);

  // Create Lower matrix
  eyes(matPtr->lower,  matPtr->dimention);

  // Loop through main matrix dimention, populate with file line 2 values
  for(int i = 0; i <  matPtr->dimention; i++)
  {
    for(int j = 0; j <  matPtr->dimention;  j++)
    {
     if( fscanf (file, "%d", &input) == EOF)
      {
        end = true;
      }
      matPtr->array[i][j] = input;
      matPtr->upper[i][j] = input;
    }
  }
   
  // Allocate memory for B and V vector
  matPtr->b = (double *)malloc( matPtr->dimention*sizeof(double*));
  matPtr->x = (double *)malloc( matPtr->dimention*sizeof(double*));

  //Read in B vector
  for(int i = 0; i <  matPtr->dimention; i++)
  {
    if( fscanf (file, "%d", &input) == EOF)
    {
      end = true;
    }
    matPtr->b[i] = input;
  }
  
  //printf("\n\n ");
  return end;
}




void eyesMPI(matrix_t * matPtr, double ** l, int offset, int rows)
{
          for (int k=0; k<rows; k++)
          {
             for (int i=0; i< matPtr->dimention; i++)
             {
                if(offset+k == i)
                {
                  l[offset+k][i] = 1;
                }
                else
                {
                  l[offset+k][i] = 0;
                }
             }
          }
}


