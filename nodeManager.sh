
function helper {
  printf "\n"
  printf "\tUsage $0 [-c|-m|-r|-v|-k]\n\n"
  printf "\t$0 -v view my current processes across nodes\n"
  printf "\t$0 -V all current processes, for program for all users across nodes\n"
  printf "\t$0 -k kill my current processes across nodes\n"
  printf "\n\n"
  exit 1 
}

if [ -z "$1" ]; then 
  helper
fi

name=`whoami| awk -F '.' '{print $1"."$2}'`
program='LUseq|LUcuda|LUomp|LUmpi|slurm'

node=(2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 18)
for i in ${node[@]}; do 
  hostname=`arp node$i | grep pvt | awk '{print $1}'`
  if [[ "$hostname" =~ .*pvt ]]; then 
    if [ "$1" = '-v' ]; then
      output="$(ssh $hostname ps -ef | grep $name| egrep -v 'ssh|ps|grep')"
      if [ ! -z "$output" ]; then 
        printf "$hostname\n"
        printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
        printf "$output\n\n"
      fi
    elif [ "$1" = '-V' ]; then
      output="$(ssh $hostname ps -ef | egrep $program | egrep -v 'ssh|ps|grep')"
      if [ ! -z "$output" ]; then 
        printf "$hostname\n"
        printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
        printf "$output\n\n"
      fi
    elif [ "$1" = "-k" ]; then 
      output="$(ssh $hostname ps -ef | grep $name| egrep -v 'ssh|ps|grep')"
      if [ ! -z "$output" ]; then 
        printf "$hostname\n"
        printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
        printf "$output\n\n"
        ssh $hostname " for i in \`ps -ef | egrep \"$program\" | grep $name | grep -v grep | awk '{print \$2}'\` ; do kill \$i ; done"
      fi
    else 
      helper
    fi 
  fi 
done
