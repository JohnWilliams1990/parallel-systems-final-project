#!/bin/bash


# Team 1 - Final Project Run Script
# UCDenver CSCI 4551 Fall 2019


# Syntax to display all output: 
#   ./run.sh

# Syntax to only display runtimes:
#   ./run.sh -r


# Run this script with the -r parameter to only display runtimes:
if [ "$1" = "-r" ]; then 
  ./run.sh -f | egrep "Size|Program"
  exit
elif [ "$1" = "-c" ]; then 
  rm -f LUomp LUseq LUcuda LUmpi
  rm -rf completed_jobs
  rm -f slurm_*
  exit
elif [ "$1" = "-h" ] || [ -z "$1" ]; then 
  printf "\n"
  printf "\n\t$0 { -c | -h | -r | -f }"
  printf "\n\t -c clean out completed jobs"
  printf "\n\t -h print this menu"
  printf "\n\t -f print full output"
  printf "\n\t -r read only output for jobs and completion times"
  printf "\n\n\n" 
  exit
fi 
   

function seq {
  sbatch c_slurm.sh $1
  printf "\n##### SEQUENTIAL $1 ##### Size: $(head -n 1 $1) #####\n"
  printit $1
}

function omp {
  sbatch openmp_slurm.sh $1 $2
  printf "\n##### OPENMP $1 ##### Size: $(head -n 1 $1) #####\n"
  printit $1
}

function mpi {
  #sbatch mpi_slurm.sh $1
  printf "\n##### MPI $1 ##### Size: $(head -n 1 $1) #####\n"
  mpirun --hostfile hosts -np 24 -ppn 24 `pwd`/LUmpi $1
  #printit $1
}

function cuda {
  sbatch gpu_slurm.sh $1 
  printf "\n##### Cuda $1 ##### Size: $(head -n 1 $1) #####\n"
  printit $1
  echo 
}

function maker {
  g++ LUseq.cpp -o LUseq
  g++ -fopenmp LUomp.cpp -o LUomp
  mpicxx LUmpi.cpp -o LUmpi
  ssh node18 "/usr/local/cuda/bin/nvcc -arch=sm_30 `pwd`/lUcuda.cu -o `pwd`/LUcuda"

  # Create directory for completed jobs if it doesn't exist. Otherwise move previously completed jobs:
  if ! [ -d "completed_jobs" ]; then
    mkdir completed_jobs
    rm -f slurm_*
  else
    if [ -f "slurm_*" ]; then 
      mv slurm_* ./completed_jobs/.
    fi 
  fi
}

function cleaner {
  rm -f LUomp LUseq LUcuda LUmpi
}

function printit {
  while [ ! -f *output* ]; do 
    sleep 1 
    printf "."
  done

  while [ -z "$( cat *output* | grep 'Program runs in ')" ]; do 
    sleep 1 
    printf "."
  done
  printf "\n"


  # cat output file and sed out results without the 
  # other info for the batch job
  cat *output* | sed '/====/q' | grep -v ====;
  
  # It's been printed, so move it to ./completed_jobs/
  mv slurm_* ./completed_jobs/.
}

if [ "$1" = "-r" ] || [ "$1" = "-f" ]; then   
  # This is the array of matrix files each program will run
  files=(
    Ax.txt
    Bx.txt
    Cx.txt
    Dx1.txt
    Dx2.txt
    Dx3.txt
    Dx4.txt
    Dx5.txt
  )
  
  # Begin script execution here:

  cleaner
  maker
  
  # For each matrix file in the array, run each version of the program
  for i in ${files[@]}; do 
    seq $i
    omp $i 4
    mpi $i
    cuda $i
  done
  cleaner  
  # End script execution
fi 
