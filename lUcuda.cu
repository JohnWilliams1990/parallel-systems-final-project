// John Williams, Brian Sumner, Amanda Suydam, Gregory Wicklund, Ryan Vacca

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <sys/time.h>
#include <cuda.h>

//#define TILE 10
//#define TILE 20
//#define TILE 25
//#define TILE 30
//#define TILE 31
#define TILE 32
//#define TILE 35
using namespace std; 

struct matrix_t{
  double **array;
  double **lower;
  double **upper;
  double *b;
  double *x;
  int dimention; 
};

double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );
void printVec(double *M , int mat );
void backSubFwdElim(matrix_t * matPtr );
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
bool read_in_file(FILE* file, matrix_t * matPtr);
void decomp(matrix_t * matPtr);
__global__ void multiplyMatrix(double* a, double* b, double* c, int n);
__global__ void LU_cuda(double* a, double* b, double* c, int n);

void launchKernel(double** ans, double **L ,double **R, int dim);

///////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]) {
  struct timeval start, end;
  double runtime;
  bool check = false;

  // file is provided on the commandline
  FILE* file = fopen( argv[1], "r");
  struct  matrix_t args;
  // scope here is inportant 
  // value of the args exists only inside the while loop
  while (!feof (file)){
    check = read_in_file(file, &args);
    if (check == true) {break;}
    gettimeofday( &start, NULL );
    decomp(&args);
    gettimeofday( &end, NULL );
    runtime = ( ( end.tv_sec  - start.tv_sec ) * 1000.0 ) + ( ( end.tv_usec - start.tv_usec ) / 1000.0 );    
    printf("Program runs in %1.8f ms\n", runtime);
    free(args.array);
    free(args.lower);
    free(args.upper);
  }
  return 0; 
}

double **alloc_2d_double(int n) {
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
    array[i] = &(data[n*i]);    
  return array;
}

void eyes( double ** matrix, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      if (i == j){
        matrix[i][j] = 1;
      }
      else {
        matrix[i][j] = 0;
      }
    }
  }
}

void cleanupMatrix(double ** arr, int dim){
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

// 0 is array. 1 is lower. 2 is upper
void printMat( matrix_t* matPtr , int mat ){
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention; j++){
      if (mat == 0){
        printf("%1.3f ", matPtr->array[i][j]);
      } else if (mat == 1){
        printf("%1.3f ", matPtr->lower[i][j]);
      } else if (mat == 2){
        printf("%1.3f ", matPtr->upper[i][j]);
      }
    }
    printf("\n");
  }
  printf("\n");
}

void printArr(double **M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    for(int j = 0; j <  mat; j++){
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

void printVec(double *M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    printf("%1.3f ", M[i]);
  }
  printf("\n");
}

bool read_in_file(FILE* file, matrix_t * matPtr){
  int input = 0;
  bool end = false;
  fscanf (file, "%d", &input);    

  if (input <= 0){ return true; /*exit(0);*/}
  matPtr->dimention = input;
  matPtr->array = alloc_2d_double( matPtr->dimention);
  matPtr->lower = alloc_2d_double( matPtr->dimention);
  matPtr->upper = alloc_2d_double( matPtr->dimention);
  eyes(matPtr->lower,  matPtr->dimention);
  for(int i = 0; i <  matPtr->dimention; i++){
    for(int j = 0; j <  matPtr->dimention;  j++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
      matPtr->array[i][j] = input;
      matPtr->upper[i][j] = input;
    }
  } 
  matPtr->b = (double *)malloc( matPtr->dimention*sizeof(double*));
  matPtr->x = (double *)malloc( matPtr->dimention*sizeof(double*));
  for(int i = 0; i <  matPtr->dimention; i++){
     if( fscanf (file, "%d", &input) == EOF){end = true;};
     matPtr->b[i] = input;
  }
  printf("\n\n ");
  return end;
}

void backSubFwdElim(matrix_t * matPtr ){
  //  Ly = b --> solve for y 
  //  Ux = y --> solve for x 

  double s = 0; 
  double y [matPtr->dimention]; 

  for (int k = 0;  k <  matPtr->dimention ; k++) {
    y[k] = matPtr->b[k];
  }

  // Ly = b
  for (int k = 0;  k <  matPtr->dimention; k++) {
    for (int i = k + 1; i < matPtr->dimention; i++) {
      //bi = bi - aik bk
      y[i] = y[i] -  matPtr->lower[i][k] * y[k];
    }
  }

  //// Ux = y 
  for(int i = matPtr->dimention-1; i > -1 ; i--){
    s = y[i];
    for (int j = i +1 ; j <  matPtr->dimention; j++){
      s = s - matPtr->upper[i][j] *  matPtr->x[j];
    }
    matPtr->x[i] = s / matPtr->upper[i][i];
  }
}

void decomp(matrix_t * matPtr){ 
  double ** l = alloc_2d_double( matPtr->dimention);
  eyes(l, matPtr->dimention);
  eyes(matPtr->lower, matPtr->dimention);

  launchKernel(matPtr->array, matPtr->lower, matPtr->upper, matPtr->dimention);

  //multiply(matPtr->lower,matPtr->array ,matPtr->array, matPtr->dimention);

  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("Matrix:\n");
  printMat( matPtr, 0);
  printf("Lower:\n");
  printMat( matPtr, 1);
  printf("Upper\n");
  printMat( matPtr, 2);
  printf("Lower * Upper\n");
  multiply(l, matPtr->lower, matPtr->upper, matPtr->dimention);
  printArr( l, matPtr->dimention);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  backSubFwdElim( matPtr );
  printVec( matPtr->x, matPtr->dimention);
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  free(l);
  //printMat( matPtr, 0);
}

void launchKernel(double** ans, double **L ,double **R, int dim){

  int n = dim;
  int numblock =  n/TILE + ((n%TILE)?1:0);
  dim3 dimGrid(numblock,numblock);
  dim3 dimBlock(TILE,TILE);	 
  double *da, *db, *dc; //device pointers

  //Allocate memory on device
  cudaMalloc((void**)&da, n*n*sizeof(double));
  cudaMalloc((void**)&db, n*n*sizeof(double));
  cudaMalloc((void**)&dc, n*n*sizeof(double));
  cudaMemcpy(da, R[0], n*n*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(db, L[0], n*n*sizeof(double), cudaMemcpyHostToDevice);

  LU_cuda <<<dimGrid,dimBlock>>>(da, db, dc,n);
  cudaThreadSynchronize();

  cudaMemcpy(L[0],db, n*n*sizeof(double),cudaMemcpyDeviceToHost); 
  cudaMemcpy(R[0],dc, n*n*sizeof(double),cudaMemcpyDeviceToHost); 
  cudaFree(da);
  cudaFree(db);
  cudaFree(dc);
}

void multiply(double** ans, double **L ,double **R, int dim){
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
}

__device__ void multiply_matrix(double* ans, double *L ,double *R, int dim){
  __shared__ double C[TILE][TILE];
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[dim*i + k] * R[dim*k+j];
      }
      ans[dim*i + j] = C[i][j];
    }
  }
}
              
__global__ void LU_cuda(double* array, double* Lower, double* Upper, int n){

  // L is assumed to be eyes at this point
  
  // indexing is done as a sequential 1D array since the 
  // multiplcation and other indexing poses issues in 2D indexing
  __shared__ double L[TILE * TILE];
  __shared__ double U[TILE * TILE];
  __shared__ double l[TILE * TILE];

  int tx = threadIdx.x;
  int ty = threadIdx.y; 
  int Row = blockIdx.y*TILE + ty;
  int Col = blockIdx.x*TILE + tx;
  ///////////////////////////
  //eyes
  if (tx == ty){ l[ty *n + tx] = 1; } else { l[ty *n + tx] = 0; }
  double R = 0;
  double Q = 0;

  // read in array for A and L 
  // L has eyes from the readfile()

  if (Row < n && Col < n){
    for (int i = 0; i < n ; i += TILE) {
      L[ n*Row + (i + tx)] = Lower[ n*Row + (i + tx)];
      U[ n*Row + (i + tx)] = array[ n*(i + ty) + Col];
       __syncthreads();
    }
  }
  
  // comparing this to sequential we can notice that the loops for sequential 
  // become if statements. pretty cool....
  if (Row < n && Col < n){
    for (int i = 0; i < n ; i += TILE) {

      // Each thread computes one element of the block sub-matrix
      int m = ((n - i) < TILE)? (n - i): TILE;
      for (int j = 0; j < m; j++){
        // for kji form j here represents k
        // eyes function for l array
        if (tx == ty){ l[ty *n + tx] = 1; } else { l[ty *n + tx] = 0; }
        __syncthreads();  
         
        if (tx >= j+1 && ty >= j) { // LOOP 2

          // eyes function
          if (tx == ty){ l[ty *n + tx] = 1; } else { l[ty *n + tx] = 0; }
          __syncthreads();

          if ( ty == j && U[j * n + j] == 1 ){
            R = U[tx * n + j];
            l[tx * n + ty] = R;
            __syncthreads();

          } else if ( ty == j && U[j * n + j] != 1){
            Q = U[j * n + j]/U[tx * n + j];
            l[tx * n + ty] = 1/Q;
            __syncthreads();
          } 
          __syncthreads();


          if ( U[j * n + j] == 1 ){
            R = U[tx * n + j];
            U[tx * n + ty] = -R * U[j * n + ty] + U[tx * n + ty];
            __syncthreads();
          } else {
            Q = U[j * n + j]/U[tx * n + j];
            U[tx * n + ty] = (-1/Q) * U[j * n + ty] + U[tx * n + ty];
            __syncthreads();
          }
          __syncthreads();

          // multiply matrix L * l here
          if ( Row == n-1) {
            //printf("I,J,K: %d %d %d\n",tx,ty,j);
            multiply_matrix(L,L ,l,n);
          }
          __syncthreads();
        }
      }
    }
    __syncthreads();
  }
  Lower[Row*n + Col] = L[Row*n + Col];
  Upper[Row*n + Col ] = U[Row*n + Col];
}


// HELPFUL SOURCES CITED
// https://solarianprogrammer.com/2019/03/27/c-programming-passing-multi-dimensional-array-to-function/

  
  // %L*A;  
  // 
  // U = A;
  // y = L\b;
  // X = L*U\L*y; 
  // end

  // backSubFwdElim(matrix_t * matPtr ) --> sudo code
  //  Ly = b --> solve for y 
  //  Ux = y --> solve for x 

  //  forward substitution
  //
  // for k = 1 to n - 1 do
  //   for i = k + 1 to n do
  //     bi = bi - aik * bk
  //   endfor
  // endfor

  //  backward elimination

  // for i = n downto 1 do
  //   s = bi
  //   for j = i + 1 to n do
  //     s = s -  aij * xj
  //   endfor
  //   xi = s/aii
  // endfor
